<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<!-- Add Admin Nav-->
@can('admin_access')

@include('admin/includes/adminnav')

@endcan

    <p>Select a Menu above.</p>

</body>
</html>