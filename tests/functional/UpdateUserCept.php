<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a user account');

// create a user in the db that we can then be deleted
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'test user',
    'email' => 'test@user.com',
    'password' => 'password',
]);

// Check that the new record exists
$I->seeRecord('users', ['name' => 'test user', 'id' => '9999']);


// Log in as Admin Dev User
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin');
$I->see('TopForm Admin', 'h1');
$I->click('Users');

// Then
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->see('test user');
// So
$I->click('test user');

//Then
$I->amOnPage('/admin/users/9999/edit');
$I->see('Edit User');

// so
$I->checkOption('#2');
$I->click('Update User and Roles');
$I->seeCurrentUrlEquals('/admin/users');
$I->seeRecord('role_user', ['role_id' => '2', 'user_id' => '9999']);
