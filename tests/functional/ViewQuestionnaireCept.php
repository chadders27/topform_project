<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('view all questionnaires');

// create dummy questionnaire
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'questionnaire1',
]);
// create dummy questions
$I->haveRecord('questions', [
    'id' => '9998',
    'text' => 'question 1',
]);

// create dummy answers
$I->haveRecord('answers', [
    'id' => '9997',
    'answer' => 'answer 1',
]);
$I->haveRecord('answers', [
    'id' => '9996',
    'answer' => 'answer 2',
]);
// Populate required pivot tables
$I->haveRecord('question_questionnaire', [
    'questionnaire_id' => '9999',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9997',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9996',
    'question_id' => '9998',
]);

// Check that the new questionnaire exist
$I->seeRecord('questionnaires', ['title' => 'questionnaire1', 'id' => '9999']);

// Log in as Admin Dev User
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin');
$I->see('TopForm Admin', 'h1');
$I->click('Questionnaires');

// Then
$I->amOnpage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('questionnaire1');
$I->click('questionnaire1');

// Then
$I->amOnPage('/admin/questionnaires/9999/edit');
$I->see('questionnaire1', 'h1');
$I->see('question 1');
$I->see('answer 1');
$I->see('answer 2');


