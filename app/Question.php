<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'text',
    ];

    /*
     * Apply relationship with Questionnaires
     */
    public function questionnaire() {
        return $this->belongsToMany('App\Questionnaire');
    }

    /*
     * Apply relationship with Answers
     */
    public function answers() {
        return $this->belongsToMany('App\Answer');
    }

    /*
     * Apply relationship with responses
     */
    public function responses() {
        return $this->belongsToMany('App\Response');
    }
}
