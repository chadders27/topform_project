<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <div class="questionnaire-title">
        <h1>{{ $questionnaire->title }}</h1>
    </div>

    <section class="questionnaire">
        <div class="row">
            <div class="large-12 columns">
            @foreach($questionnaire->questions as $question)
                <div class="row question">
                    <div class="large-12 columns"><p></p></div>
                    <div class="large-12 columns">
                        {{ $question->text }}
                     </div>
                    <div class="large-12 columns"><p></p></div>
                    <div class="large-12 columns"><p></p></div>
                    @foreach($question->answers as $answer)
                    <div class="large-2 columns"><p></p></div>
                    <div class="large-2 columns">
                        <p>{{ $answer->answer }}</p>
                    </div>
                    <div class="large-12 columns"><p></p></div>
                    @endforeach
                </div>
            @endforeach
            </div>
        </div>
    </section>
    <div class="large-6 small-6 columns">
        <a href="/admin/questionnaires"><button class="button success round">back to Questionnaire List</button></a>
    </div>
    <div class="large-4 small-4 columns">
        {!! Form::open(['method' => 'DELETE', 'action' => ['AdminQuestionnaireController@destroy', $questionnaire->id]]) !!}
            {!! Form::submit('Delete Questionnaire', ['class' => 'button right alert small radius']) !!}
        {!! Form::close() !!}
    </div>

</body>
</html>