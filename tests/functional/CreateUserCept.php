<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new user');

// Log in as Admin Dev User
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin');
$I->see('TopForm Admin', 'h1');
$I->click('Users');

// Then
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->dontSee('Ted Edwards');
// So
$I->click('Add User');

// Then
$I->amOnPage('/admin/users/create');
$I->see('Add User', 'h1');
$I->submitForm('#createuser', [
    'name' => 'Ted Edwards',
    'email' => 'tededwards@example.com',
    'password' => 'password'
]);

// Then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('Ted Edwards');