<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add Dev User

        DB::table('users')->insert([
            ['id' => 1, 'name' => "tom chadwick",
                'email' => 'tomchadwick@example.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            ['id' => 2, 'name' => "Admin Dev",
                'email' => 'admin@example.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
            ['id' => 3, 'name' => "Researcher",
                'email' => 'researcher@example.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(10),
            ],
        ]);
    }
}
