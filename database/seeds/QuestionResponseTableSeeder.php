<?php

use Illuminate\Database\Seeder;

class QuestionResponseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_response')->insert([
            ['question_id' => 1, 'response_id' => 1,],
            ['question_id' => 1, 'response_id' => 2,],
            ['question_id' => 1, 'response_id' => 3,],
            ['question_id' => 2, 'response_id' => 4,],
            ['question_id' => 2, 'response_id' => 5,],
            ['question_id' => 2, 'response_id' => 6,],
            ['question_id' => 3, 'response_id' => 7,],
            ['question_id' => 3, 'response_id' => 8,],
            ['question_id' => 3, 'response_id' => 9,],
            ['question_id' => 4, 'response_id' => 10,],
            ['question_id' => 4, 'response_id' => 11,],
            ['question_id' => 4, 'response_id' => 12,],
            ['question_id' => 5, 'response_id' => 13,],
            ['question_id' => 5, 'response_id' => 14,],
            ['question_id' => 5, 'response_id' => 15,],
            ['question_id' => 6, 'response_id' => 16,],
            ['question_id' => 6, 'response_id' => 17,],
            ['question_id' => 6, 'response_id' => 18,],
            ['question_id' => 7, 'response_id' => 19,],
            ['question_id' => 7, 'response_id' => 20,],
            ['question_id' => 7, 'response_id' => 21,],
            ['question_id' => 8, 'response_id' => 22,],
            ['question_id' => 8, 'response_id' => 23,],
            ['question_id' => 8, 'response_id' => 24,],
            ['question_id' => 9, 'response_id' => 25,],
            ['question_id' => 9, 'response_id' => 26,],
            ['question_id' => 9, 'response_id' => 27,],
        ]);
    }
}
