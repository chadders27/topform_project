<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 *  Assign Relationships to Questionnaires
 *  -   Relationship between Questionnaires & Questions
 *  -   Relationship between Questionnaires & Users
 */
class Questionnaire extends Model
{

    protected $fillable = [
        'title',
    ];

    // Has Many Questions
    public function questions() {
        return $this->belongsToMany('App\Question');
    }

    // Has only one User
    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
