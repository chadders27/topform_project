<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Question</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>New Question</h1>
<div>
    {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
    {{  csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('text', 'Question:') !!}
        {!! Form::text('text', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div>
        {!! Form::hidden('id['.$questionnaire_id.']') !!}
    </div>
    <div class="row large-4 columns">
        {!! Form::submit('Create Question', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}
</div>

</body>
</html>