<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="questionnaire-title">
    <h1>{{ $questionnaire->title }}</h1>
</div>
{!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => '/questionnaires/'. $questionnaire->id]) !!}

<div>
    {!! Form::label('title', 'Title: ') !!}
    {!! Form::text('title', null) !!}
</div>
<div class="large-4 columns">
    {!! Form::submit('Update Title', ['class' => 'button small round edit-button']) !!}
</div>
{!! Form::close() !!}


<section class="questionnaire">
    <div class="row">
        <div class="large-12 columns">
            @foreach($questionnaire->questions as $question)
                <div class="row question">
                    <div class="large-12 columns"><p></p></div>
                    <div class="large-10 columns">
                        {{ $question->text }}
                    </div>
                    <div class="large-2 columns"><a href="/questions/{{ $question->id }}/edit"><button class="edit-button round small">Edit Question</button></a></div>
                    <div class="large-12 columns"><p></p></div>
                    <div class="large-12 columns"><p></p></div>
                    @foreach($question->answers as $answer)
                        <div class="large-2 columns"><p></p></div>
                        <div class="large-8 columns">
                            <h4 class="question_choices">{{ $answer->answer }}</h4>
                        </div>
                        <div class="large-2 columns"><a href="/answers/{{ $answer->id }}/edit"><button class="edit-button round small">Edit Answer</button></a></div>
                        <div class="large-12 columns"><p></p></div>
                    @endforeach
                    <div class="large-12 columns">
                        <a href="/answers/{{$questionnaire->id}}/{{ $question->id }}/create"><button class="edit-button round small">Add Answer</button></a>
                    </div>
                    <div class="large-12 columns"><p></p></div>
                </div>
            @endforeach
        </div>
        <div class="large-5 columns"><p></p></div>
        <div class="large-4 columns">
            <a href="/questions/{{$questionnaire->id}}/create"><button class="edit-button round small">Add Question</button></a>
        </div>
        <div class="large-3 columns"><p></p></div>
    </div>
</section>
<div class="large-6 small-6 columns">
    <a href="/home"><button class="button success round">back to Questionnaire List</button></a>
</div>
<div class="large-4 small-4 columns">
    {!! Form::open(['method' => 'DELETE', 'action' => ['QuestionnaireController@destroy', $questionnaire->id]]) !!}
    {!! Form::submit('Delete Questionnaire', ['class' => 'button right alert small radius']) !!}
    {!! Form::close() !!}
</div>

</body>
</html>