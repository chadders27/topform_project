<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'answer',
    ];

    /*
     * Apply relationship with Questions
     */
    public function question() {
        return $this->belongsToMany('App\Question');
    }

    /*
     * Apply relationship with Responses
     */
    public function response() {
        return $this->belongsToMany('App\Response');
    }
}
