<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $questionnaire->title }}</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="questionnaire-title">
    <h1>{{ $questionnaire->title }}</h1>
</div>
{!! Form::open(array('action' => 'ResponseController@store')) !!}
<section class="questionnaire">
    <div class="row">
        <div class="large-12 columns">
            @foreach($questionnaire->questions as $question)
                <div class="row question">
                    <div class="large-12 columns"><p></p></div>
                    <div class="large-12 columns">
                        {{ $question->text }}
                    </div>
                    <div class="large-12 columns"><p></p></div>
                    @foreach($question->answers as $answer)
                        <div class="answer">
                            {!! Form::label($answer->answer.$question->id, $answer->answer) !!}
                            {!! Form::radio('qid['.$question->id.']', $answer->answer) !!}
                            {!! Form::hidden('aid['.$answer->id.']') !!}
                        </div>
                    @endforeach
                    <div class="large-12 columns"><p></p></div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<div class="large-5 columns"><a href="/"><button class="button alert round large">Quit</button></a></div>
<div class="large-4 columns">
    {!! Form::submit('Submit', ['class' => 'button round success large']) !!}
</div>
<div class="large-3 columns"><p></p></div>
{!! Form::close() !!}

</body>
</html>