<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit User</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <h1>Edit User - {{ $user->name }}</h1>

    <div class="row">
    {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}

    <div>
        {!! Form::label('name', 'Name: ') !!}
        {!! Form::text('name', null) !!}
    </div>

    <div>
        {!! Form::label('email', 'Email Address: ') !!}
        {!! Form::text('email', null) !!}
    </div>

    <div>
        {!! Form::label('roles', 'Roles: ') !!}
        @foreach($roles as $role)
            <div class=" large-12 columns">
            {{ Form::label($role->name) }}
            {{ Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id]) }}
            </div>
        @endforeach

    </div>

        <div class="large-4 columns">
            {!! Form::submit('Update User and Roles', ['class' => 'button']) !!}
        </div>
    {!! Form::close() !!}
        <div class="large-8 columns">
            {!! Form::open(['method' => 'DELETE', 'action' => ['UserController@destroy', $user->id]]) !!}
                {!! Form::submit('Delete User', ['class' => 'button alert']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</body>
</html>