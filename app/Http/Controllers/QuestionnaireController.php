<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Questionnaire;
use App\Http\Requests;
use App\User;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questionnaires/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionnaire = Questionnaire::create($request->all());
        $user = Auth::user();
        $questionnaire->user()->attach($user->id);
        $questionnaire->save();

        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //get the questionnaire
        $questionnaire = Questionnaire::where('id', $id)->first();

        // if questionnaire does not exist return to list
        if(!$questionnaire)
        {
            return redirect('/home');
        }
        return view('/questionnaires/show', compact('questionnaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get the questionnaire
        $questionnaire = Questionnaire::where('id', $id)->first();

        // if questionnaire does not exist return to list
        if(!$questionnaire)
        {
            return redirect('/home');
        }
        return view('questionnaires/edit')->with('questionnaire', $questionnaire);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $questionnaire = Questionnaire::findOrFail($id);

        $questionnaire->update($request->all());
        $questionnaire->save();

        return redirect('/questionnaires/' . $questionnaire->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::find($id);

        $questionnaire->delete();

        return redirect('/home');
    }
}
