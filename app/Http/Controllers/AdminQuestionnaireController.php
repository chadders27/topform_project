<?php

namespace App\Http\Controllers;
use Gate;
use Illuminate\Http\Request;
use App\Questionnaire;
use App\User;
use App\Http\Requests;

class AdminQuestionnaireController extends Controller
{
    /**
     * Apply Auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //If admin
        if (Gate::allows('rud_questionnaires')){

            $questionnaires = Questionnaire::all();
            $users = User::all();
            return view('/admin/questionnaires/index', ['questionnaires' => $questionnaires])->with('users', $users);
        }
        return view('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get the questionnaire
        $questionnaire = Questionnaire::where('id', $id)->first();

        // if questionnaire does not exist return to list
        if(!$questionnaire)
        {
            return redirect('/admin/questionnaires');
        }
        return view('admin/questionnaires/edit')->with('questionnaire', $questionnaire);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::find($id);

        $questionnaire->delete();

        return redirect('/admin/questionnaires');
    }
}
