<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Questionnaire</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <h1>New Questionnaire</h1>
    <div>
        {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
        {{  csrf_field() }}
        <div class="row large-12 columns">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
        </div>
        <div class="row large-4 columns">
            {!! Form::submit('Create Questionnaire', ['class' => 'button']) !!}
        </div>
        {!! Form::close() !!}
    </div>

</body>
</html>