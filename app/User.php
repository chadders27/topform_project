<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * Apply relationship with Roles
     */
    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    /*
     * Check if the user has a role assigned
     */
    public function hasRole($role) {
        if (is_string($role)){
            return $this->roles->contains('name', $role);
        }
        return !! $role->intersect($this->roles)->count();
    }

    /*
     * Assign a role to the user
     */
    public function assignRole($role) {
        return $this->roles()->sync(
          Role::whereName($role)->firstOrFail()
        );
    }

    /*
     * Apply relationship with Questionnaires
     */
    public function questionnaires() {
        return $this->belongsToMany('App\Questionnaire');
    }

    /*
     * Assign a questionnaire to the user
     */
    public function assignQuestionnaire($questionnaire) {
        return $this->questionnaires()->attach(
            Questionnaire::whereName($questionnaire)->firstOrFail()
        );
    }

}
