<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'answer_id',
    ];

    /*
     * Apply relationship with Questions
     */
    public function question() {
        return $this->belongsToMany('App\Question');
    }

    /*
     * Apply relationship with Answers
     */
    public function answers() {
        return $this->belongsToMany('App\Answer');
    }
}
