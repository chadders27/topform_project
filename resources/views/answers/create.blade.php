<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Answer</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>New Question</h1>
<div>
    {!! Form::open(array('action' => 'AnswerController@store', 'id' => 'createanswer')) !!}
    {{  csrf_field() }}
    <div class="row large-12 columns">
        {!! Form::label('answer', 'Answer:') !!}
        {!! Form::text('answer', null, ['class' => 'large-8 columns']) !!}
    </div>
    <div>
        {!! Form::hidden('id['.$questionnaire_id.']') !!}
    </div>
    <div>
        {!! Form::hidden('id2['.$question_id.']') !!}
    </div>
    <div class="row large-4 columns">
        {!! Form::submit('Create Answer', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}
</div>

</body>
</html>