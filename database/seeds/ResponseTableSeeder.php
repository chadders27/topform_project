<?php

use Illuminate\Database\Seeder;

class ResponseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('responses')->insert([
            ['id' => 1, 'answer_id' => 1,],
            ['id' => 2, 'answer_id' => 2,],
            ['id' => 3, 'answer_id' => 2,],
            ['id' => 4, 'answer_id' => 4,],
            ['id' => 5, 'answer_id' => 4,],
            ['id' => 6, 'answer_id' => 4,],
            ['id' => 7, 'answer_id' => 7,],
            ['id' => 8, 'answer_id' => 6,],
            ['id' => 9, 'answer_id' => 6,],
            ['id' => 10, 'answer_id' => 8,],
            ['id' => 11, 'answer_id' => 8,],
            ['id' => 12, 'answer_id' => 8,],
            ['id' => 13, 'answer_id' => 10,],
            ['id' => 14, 'answer_id' => 11,],
            ['id' => 15, 'answer_id' => 11,],
            ['id' => 16, 'answer_id' => 13,],
            ['id' => 17, 'answer_id' => 13,],
            ['id' => 18, 'answer_id' => 13,],
            ['id' => 19, 'answer_id' => 14,],
            ['id' => 20, 'answer_id' => 14,],
            ['id' => 21, 'answer_id' => 14,],
            ['id' => 22, 'answer_id' => 17,],
            ['id' => 23, 'answer_id' => 16,],
            ['id' => 24, 'answer_id' => 16,],
            ['id' => 25, 'answer_id' => 18,],
            ['id' => 26, 'answer_id' => 18,],
            ['id' => 27, 'answer_id' => 18,],
        ]);
    }
}
